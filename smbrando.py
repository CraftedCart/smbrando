#!/usr/bin/env python3

import re
import random
import glob
import os
import sys
from typing import List

beginner_stages = [
    361, 362, 363, 364, 205, 206, 207, 208, 209, 210
]

beginner_extra_stages = [
    211, 212, 213, 214, 215, 216, 217, 218, 219, 220
]

advanced_stages = [
    221, 222, 223, 224, 225, 226, 227, 228, 369, 230,
    371, 372, 373, 374, 375, 376, 377, 378, 379, 240,
    241, 242, 243, 244, 245, 246, 247, 248, 249, 250
]

advanced_extra_stages = [
    251, 252, 253, 254, 255, 256, 257, 258, 259, 260
]

expert_stages = [
    261, 262, 263, 264, 265, 266, 267, 268, 269, 270,
    271, 272, 273, 274, 275, 276, 277, 278, 279, 280,
    381, 382, 383, 384, 385, 386, 387, 388, 389, 290,
    291, 292, 293, 294, 295, 296, 297, 298, 299, 300,
    301, 302, 303, 304, 305, 306, 307, 308, 309, 310,
]

expert_extra_stages = [
    311, 312, 313, 314, 315, 316, 317, 318, 319, 320
]

master_stages = [
    321, 322, 323, 324, 325, 326, 327, 328, 329, 330
]

master_extra_stages = [
    331, 332, 333, 334, 335, 336, 337, 338, 339, 390
]

story_stages = [
    201, 202, 203, 204,   1,   2,   3,   4,   5,   6,
      7,   8,   9,  10,  11,  12,  13,  14, 359,  16,
    231, 232, 233, 234, 235, 236, 237, 238, 239,  17,
     18,  19,  20,  21,  22,  23,  24,  25,  26,  27,
     28,  29,  30,  31,  32,  33,  34,  35,  36,  37,
     38,  39,  40,  41,  42,  43,  44,  45,  46,  47,
    281, 282, 283, 284, 285, 286, 287, 288, 289,  48,
     49,  50,  51,  52,  53,  54,  55,  56,  57,  58,
     59,  60,  61,  62,  63,  64,  65,  66,  67,  68,
    341, 342, 343, 344, 345, 346, 347, 348, 349, 350
]

from_lz_regex = re.compile(r"(.*)STAGE(...)\.lz")

def main():
    if "--help" in sys.argv:
        print_help()
        return

    if os.path.isdir("smbrando-input"):
        lz_files = glob.glob("smbrando-input/**/STAGE*.lz", recursive = True)
        print("Found %d stages to randomize" % len(lz_files))

        randomized_b = False
        randomized_bx = False
        randomized_a = False
        randomized_ax = False
        randomized_e = False
        randomized_ex = False
        randomized_m = False
        randomized_mx = False
        randomized_story = False

        if not os.path.exists("smbrando-output"):
            os.mkdir("smbrando-output")

        if "--beginner" in sys.argv:
            print("Randomizing Beginner")
            print("====================")
            randomized_b = randomize_set(lz_files, beginner_stages)
            print()
        if "--beginner-extra" in sys.argv:
            print("Randomizing Beginner Extra")
            print("==========================")
            randomized_bx = randomize_set(lz_files, beginner_extra_stages)
            print()
        if "--advanced" in sys.argv:
            print("Randomizing Advanced")
            print("====================")
            randomized_a = randomize_set(lz_files, advanced_stages)
            print()
        if "--advanced-extra" in sys.argv:
            print("Randomizing Advanced Extra")
            print("==========================")
            randomized_ax = randomize_set(lz_files, advanced_extra_stages)
            print()
        if "--expert" in sys.argv:
            print("Randomizing Expert")
            print("==================")
            randomized_e = randomize_set(lz_files, expert_stages)
            print()
        if "--expert-extra" in sys.argv:
            print("Randomizing Expert Extra")
            print("========================")
            randomized_ex = randomize_set(lz_files, expert_extra_stages)
            print()
        if "--master" in sys.argv:
            print("Randomizing Master")
            print("==================")
            randomized_m = randomize_set(lz_files, master_stages)
            print()
        if "--master-extra" in sys.argv:
            print("Randomizing Master Extra")
            print("========================")
            randomized_mx = randomize_set(lz_files, master_extra_stages)
            print()
        if "--story" in sys.argv:
            print("Randomizing Story")
            print("=================")
            randomized_story = randomize_set(lz_files, story_stages)
            print()

        print("Summary")
        print("=======")
        print("Randomized Beginner: %s" % randomized_b)
        print("Randomized Beginner Extra: %s" % randomized_bx)
        print("Randomized Advanced: %s" % randomized_a)
        print("Randomized Advanced Extra: %s" % randomized_ax)
        print("Randomized Expert: %s" % randomized_e)
        print("Randomized Expert Extra: %s" % randomized_ex)
        print("Randomized Master: %s" % randomized_m)
        print("Randomized Master Extra: %s" % randomized_mx)
        print("Randomized Story: %s" % randomized_story)
        print()
        print("Randomized stages have been placed in smbrando-output/")
        print("This expects that Mechalico's mkb2.main_loop.rel has been used (Use --help for a download link)")
    else:
        os.mkdir("smbrando-input")
        print("No smbrando-input directory found, so one has been created for you", file = sys.stderr)
        print("Place stage files (st*.gma/st*.tpl/STAGE*.lz) into smbrando-input/** then rerun the script", file = sys.stderr)
        print(file = sys.stderr)
        print_help()

def randomize_set(lz_files: List[str], stage_set: List[int]) -> bool:
    if (len(lz_files) < len(stage_set)):
        print("Not enough remaining input files to cover this set!", file = sys.stderr)
        return False
    else:
        i = 0
        while i < len(stage_set):
            lz_file = random.choice(lz_files)
            lz_out_file = os.path.join("smbrando-output", "STAGE%03d.lz" % stage_set[i])
            lz_files.remove(lz_file)

            gma_file = from_lz_regex.sub(r"\1st\2.gma", lz_file)
            gma_out_file = os.path.join("smbrando-output", "st%03d.gma" % stage_set[i])
            tpl_file = from_lz_regex.sub(r"\1st\2.tpl", lz_file)
            tpl_out_file = os.path.join("smbrando-output", "st%03d.tpl" % stage_set[i])

            if not os.path.exists(gma_file):
                print("Warning: GMA file %s does not exist - trying another file" % gma_file, file = sys.stderr)
                continue
            if not os.path.exists(tpl_file):
                print("Warning: TPL file %s does not exist - trying another file" % tpl_file, file = sys.stderr)
                continue

            os.rename(lz_file, lz_out_file)
            os.rename(gma_file, gma_out_file)
            os.rename(tpl_file, tpl_out_file)

            print("Stage %3d (st%03d): %s, %s, %s" % (i + 1, stage_set[i], lz_file, gma_file, tpl_file))

            i += 1

        return True

def print_help():
    print("Usage")
    print("=====")
    print()
    print("%s [options]" % sys.argv[0])
    print()
    print("--help              Display this help text")
    print("--beginner          Randomize the beginner difficulty")
    print("--beginner-extra    Randomize the beginner extra difficulty")
    print("--advanced          Randomize the advanced difficulty")
    print("--advanced-extra    Randomize the advanced extra difficulty")
    print("--expert            Randomize the expert difficulty")
    print("--expert-extra      Randomize the expert extra difficulty")
    print("--master            Randomize the master difficulty")
    print("--master-extra      Randomize the master extra difficulty")
    print("--story             Randomize story mode")
    print()
    print("smbrando.py will take stages in smbrando-input/** and move/rename randomly selected ones in a random order into smbrando-output/")
    print("Subdirectories in smbrando-input/ are supported - this place is searched recursively for stage files")
    print("Input stage filenames must be in the format of st*.gma/st*.tpl/STAGE*.lz")
    print("It doesn't matter if there are more than 420 stages in the input directory")
    print()
    print("Note that smbrando.py expects that Mechalico's REL is used")
    print("A download for that can be found at https://cdn.discordapp.com/attachments/238040800070598656/326462917543198730/240stagemainloop2.zip")

if __name__ == "__main__":
    main()

