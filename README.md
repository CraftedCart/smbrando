smbrando
========

A stage randomizer for Super Monkey Ball 2

### Usage

- Make a `smbrando-input/` directory or run smbrando.py to generate one for you
- Place stage files (STAGE*.lz/st*.gma/st*.tpl) into the directory (They can be in subdirectories - this place is searched recursively)
- Run smbrando.py with the appropriate switches to select what to randomize
- The script will randomly select stages and move/rename them into `smbrando-output/`
- You can then patch the game by putting these files into `stage` in a Super Monkey Ball 2 root (Also make sure you're using Mechalico's mkb2.main_loop.rel file)


### Output of --help
```
$ ./smbrando.py --help
Usage
=====

./smbrando.py [options]

--help              Display this help text
--beginner          Randomize the beginner difficulty
--beginner-extra    Randomize the beginner extra difficulty
--advanced          Randomize the advanced difficulty
--advanced-extra    Randomize the advanced extra difficulty
--expert            Randomize the expert difficulty
--expert-extra      Randomize the expert extra difficulty
--master            Randomize the master difficulty
--master-extra      Randomize the master extra difficulty
--story             Randomize story mode

smbrando.py will take stages in smbrando-input/** and move/rename randomly selected ones in a random order into smbrando-output/
Subdirectories in smbrando-input/ are supported - this place is searched recursively for stage files
Input stage filenames must be in the format of st*.gma/st*.tpl/STAGE*.lz
It doesn't matter if there are more than 420 stages in the input directory

Note that smbrando.py expects that Mechalico's REL is used
A download for that can be found at https://cdn.discordapp.com/attachments/238040800070598656/326462917543198730/240stagemainloop2.zip

```

